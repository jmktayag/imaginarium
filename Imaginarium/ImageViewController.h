//
//  ImageViewController.h
//  Imaginarium
//
//  Created by Mckein on 12/11/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController

@property (nonatomic, strong) NSURL *imageURL; //url of image to be passed

@end
